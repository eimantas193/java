public class Customer
{
    public static void main (String args[])
    {
        int custID = 44500;
        final int SSN = 567889900;
        char custStatus = 'T';
        boolean memberFB = false;
        float yPurchases = 1394.90F;
        
        System.out.println("Customer ID is: " + 44500);
        System.out.println("Social security number is: " + SSN);
        System.out.println("Customer status is: " + custStatus);
        System.out.println("Customer is a member of Frequent Buyers' Club? " + memberFB);
        System.out.println("Customer yearly purchases were: " + yPurchases);
        
        System.out.println("Average purchases each month are: " + (yPurchases/12));
    }
}
        

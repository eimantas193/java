public class AktyviejiFiltraiPagrindine
{
    static void output(AktyviejiFiltraiKlase klase)
   {
    System.out.println("Filtro tipas: "+klase.filtro_tipas);
    System.out.println("Filtro ribinis dažnis: "+klase.ribinis_dazinis+"Hz");
    System.out.println("Vidinė varža: "+klase.vid_varza+" omų");
    System.out.println("Autonominis maitinimas: "+klase.aut_maitinimas+"\n");
   }
   public static void main (String args[])
   {
    AktyviejiFiltraiKlase filtras1 = new AktyviejiFiltraiKlase();
    AktyviejiFiltraiKlase filtras2 = new AktyviejiFiltraiKlase();
    AktyviejiFiltraiKlase filtras3 = new AktyviejiFiltraiKlase();
      filtras1.filtro_tipas = "zemu dazniu";
      filtras1.ribinis_dazinis = 25.5F;
      filtras1.vid_varza = 100;
      filtras1.aut_maitinimas = true;

      filtras2.filtro_tipas = "aukstu dazniu";
      filtras2.ribinis_dazinis = 100.5F;
      filtras2.vid_varza = 10;
      filtras2.aut_maitinimas = true;

      filtras3.filtro_tipas = "juostinis";
      filtras3.ribinis_dazinis = 75.5F;
      filtras3.vid_varza = 50;
      filtras3.aut_maitinimas = false;

      output(filtras1);
      output(filtras2);
      output(filtras3);
   }
}
public class ReaktyvineVarza
{
    public static void main(String args[])
    {
        int state;
        String tekstas;
        while (true)
        {
            System.out.println("Jei norima apskaiciuoti kondencatoriaus reaktyvine varza ivesti 0, jei rites 1");
            tekstas = System.console().readLine(); 
            state = Integer.parseInt(tekstas);
            if (state==0)
            {
                double talpa, daznis, rez;
                System.out.println("Iveskite kondencatoriaus talpa");
                tekstas = System.console().readLine(); 
                talpa = Double.parseDouble(tekstas);
                System.out.println("Iveskite kondencatoriaus dazni");
                tekstas = System.console().readLine(); 
                daznis = Double.parseDouble(tekstas);
                rez=1/(2*Math.PI*talpa*daznis);
                System.out.println("Induktyvioji kondencarotiaus varza: " + rez+ " omai");
                break;
            }
            else if (state==1)
            {
                double induktyvumas, daznis, rez;
                System.out.println("Iveskite rites induktyvuma");
                tekstas = System.console().readLine(); 
                induktyvumas = Double.parseDouble(tekstas);
                System.out.println("Iveskite rites dazni");
                tekstas = System.console().readLine(); 
                daznis = Double.parseDouble(tekstas);
                rez = 2*Math.PI*daznis*induktyvumas;
                System.out.println("Induktyvioji rites varza: " + rez +" omai");
                break;
            }
            else
            {
                System.out.println("Nera tokio pasirinkimo. Bandykite dar karta");
            }
        }
    }
}
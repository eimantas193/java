/**
 * Petersons.java
 *
 * This program implements strict alternation as a means of handling synchronization.
 *
 * Note - Using an array for the two flag variables would be preferable, however  we must
 * declare the data as volatile and volatile does not extend to arrays.
 */


 public class Petersons implements MutualExclusion // Petersono algoritmo įgyvendinimas 
 {
   private volatile int turn; // inicelizuojamas ini klasės kintamasis turn skirtas reguliuot įeinančius procesus į kritinę sekciją
   private volatile boolean flag0; // inicelizuojamas boolean klasės kinatamsis skirtas rodyti pirmo proceso būseną
   private volatile boolean flag1; // inicelizuojamas boolean klasės kinatamsis skirtas rodyti antra proceso būseną

   public Petersons() {
	flag0 = false; // pirmo proceso vėlevėle nuleidžaima 
	flag1 = false; // antro proceso vėlevėlė nuleidžaima

	turn = 0; // eilės numeris
   }

   public void entrySection(String name, int t) {  // įėjimo į kritinę sekciją funkcija
		int other = 1 - t; // gaunamas eilės id

		if (t == 0) { // įeinama jei pirmas procesas
			turn = other; // pakeičiama "turn" vertė leidžianti kitam procesui įeiti į kirtinę sekciją
			flag0 = true; // pakeliama vėlėvėlė, kad norima eiti į kritinę sekciją
			System.out.println(name + " flag " +flag0);
			while ( (flag1 == true) && (turn == other) ) // sukamas tuščias while iki kitas procesas baigs darbą kritinei sekcijoje ir pakeis turn vertę
				Thread.yield();// grąžina resursus planuotojui, bet tuo pačiu reiklauja, kad kuo greičiau būtų vėl grąžintas į veikimą 
		}
		else { // įeinama jei antras procesas
			turn = other; // pakeičiama "turn" vertė leidžianti kitam procesui įeiti į kirtinę sekciją
			flag1 = true; // pakeliama vėlėvėlė, kad norima eiti į kritinę sekciją
			System.out.println(name + " flag " +flag1);
			while ( (flag0 == true) && (turn == other) ) // sukamas tuščias while iki kitas procesas baigs darbą kritinei sekcijoje ir pakeis turn vertę
				Thread.yield(); // grąžina resursus planuotojui, bet tuo pačiu reiklauja, kad kuo greičiau būtų vėl grąžintas į veikimą 
		}
   }

   public void exitSection(String name,int t) { // išėjimas iš kritinės sekcijos
		if(t == 0) // įeinama jei pirmas procesas
		{
			flag0 = false; // nuleidžiama vėlevėlė, kad išeinama iš kritinės sekcijos
			System.out.println(name + " flag " +flag0);
		}
		else // įeinama jei antras procesas
		{
			flag1 = false; // nuleidžiama vėlevėlė, kad išeinama iš kritinės sekcijos
			System.out.println(name + " flag " +flag1);
		}
			
   }
}

public class RealiujuSudetis implements Runnable 
{
    private float upper;	
    private MutableFloat sumValue; 

    public RealiujuSudetis(float upper, MutableFloat sumValue)
    {
    if (upper < 0)
        throw new IllegalArgumentException();

    this.upper = upper;
    this.sumValue = sumValue;
    }

    public void run()
    {
        float sum = 0, i = 0;
        while (i<=upper)
        {
            sum+=i;
            i+=0.5;
        }
        sumValue.set(sum);
    }	
}
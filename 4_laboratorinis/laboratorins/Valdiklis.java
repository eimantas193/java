public class Valdiklis	// Pagrindinė klasė
{
    public static void main(String[] args) // Metodas ,main"
    {
        if (args.length != 1) 
        {
        System.err.println("Usage Driver <integer>"); 
        System.exit(0);
        }
        MutableFloat sumObject1 = new MutableFloat();
        MutableInteger sumObject2 = new MutableInteger();
        float upper1 =Float.parseFloat(args[0]);
        int upper2 =Integer.parseInt(args[0]);
        Thread worker1 = new Thread(new RealiujuSudetis(upper1, sumObject1));
        Thread worker2 = new Thread(new Summation(upper2, sumObject2));
        worker1.start();
        worker2.start();
        try {
        worker1.join();
        worker2.join();
        } catch (InterruptedException ie) {}
        System.out.println("The value of " + upper1 + " is " + sumObject1.get());
        System.out.println("The value of " + upper2 + " is " + sumObject2.get());
    }
}
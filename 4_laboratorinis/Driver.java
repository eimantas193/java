public class Driver	// Pagrindinė klasė
{
    public static void main(String[] args) // Metodas ,main"
    {
        if (args.length != 1) 
        {
        System.err.println("Usage Driver <integer>"); 
        System.exit(0);
        }
        MutableInteger sumObject = new MutableInteger();
        int upper =Integer.parseInt(args[0]);
        Thread worker = new Thread(new Summation(upper, sumObject));
        worker.start();
        try {
        worker.join();
        } catch (InterruptedException ie) {}
        System.out.println("The value of " + upper + " is " + sumObject.get());
    }
}

public class Filtras3
{
    public String fil_tip;
    public int prad_daz;
    public int rib_daz;
    public int rez;
    public void print(int pasirinkimas)
    {
        if(pasirinkimas == 0)
        {
            System.out.println("Filtros tipas: " + this.fil_tip);
        }
        else if(pasirinkimas == 1)
        {
            System.out.println("Filtros tipas: " + this.fil_tip + "\n" + "Pradinis dažnis: " + this.prad_daz+"Hz");
        }
        else if(pasirinkimas == 2)
        {
            System.out.println("Filtros tipas: " + this.fil_tip + "\n" + "Pradinis dažnis: " + this.prad_daz+"Hz\n"+"Ribinis dažnis: "+this.rib_daz+"Hz");
        }
        else if(pasirinkimas == 3)
        {
            System.out.println("Filtros tipas: " + this.fil_tip + "\n" + "Pradinis dažnis: " + this.prad_daz+"Hz\n"+"Ribinis dažnis: "+this.rib_daz+"Hz\n"+"Darbo dažnio juosta: " + String.valueOf(this.rib_daz-this.prad_daz) + "Hz");
        }
        else
        {
            System.out.println("Blogas pasirinkimas.");
        }
    }
    public void set_Fil_tip(String nauja_verte)
    {
	
        this.fil_tip = nauja_verte;
	
    }
    public void set_prad_daz(int nauja_verte)
    {
	
        this.prad_daz = nauja_verte;
	
    }
    public void set_rib_daz(int nauja_verte)
    {
	
        this.rib_daz = nauja_verte;
	
    }
}
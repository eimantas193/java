public class Dazniai
{
    public static String daz_tip(long daznis)
    {
        if (daznis >= 3 && daznis <30)
        {
            return "labai žemas";
        }
        else if (daznis >= 30 && daznis < 300)
        {
            return "žemasis";
        }
        else if (daznis >= 300 && daznis < 3000)
        {
            return "vidutinis";
        }
        else if (daznis >= 3000 && daznis < 30000)
        {
            return "aukštasis";
        }
        else if (daznis >= 30000 && daznis < 300000)
        {
            return "labai aukštas";
        }
        else if (daznis >= 300000 && daznis < 3000000)
        {
            return "ultraaukštas";
        }
        else if (daznis >= 3000000 && daznis < 300000000L)
        {
            return "superaukštas";
        }
        else if (daznis >= 300000000L && daznis < 3000000000L)
        {
            return "ypač aukštas";
        }
        else if (daznis >= 3000000000L && daznis < 30000000000L)
        {
            return "hiperaukštas";
        }
        else
        {
            return "kiti dažniai";
        }
    }
}
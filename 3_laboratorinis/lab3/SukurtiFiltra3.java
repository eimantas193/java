public class SukurtiFiltra3
{
    public static void main(String args[])
    {
        int pasirinkimas;
        Filtras3 naujas_filtras = new Filtras3();
		naujas_filtras.set_Fil_tip("ŽDF");
        naujas_filtras.set_prad_daz(400);
        naujas_filtras.set_rib_daz(800);

        System.out.println("0 - tik filtro tipas\n1 - filtro tipas ir pardinis daznis\n2 - filtro tipas, pradinis daznis ir ribinis daznis\n3 - filtro tipas, pradinis daznis, ribinis daznis ir darbo dažnių juosta\n");
		pasirinkimas = Integer.parseInt(System.console().readLine());
        naujas_filtras.print(pasirinkimas);
    }// end main

}//end class